// always put your angular module in a IIFE
// this makes sure that we don't pollute global variables
(function(){
    console.log(">> index.js loaded");

    // create the module named: shoppingapp
    var app = angular.module('shoppingapp',[]);
    console.log(">> app: " + app.name);

    // create a controller named: MainCtrl
    // The controller has 2 dependencies: $http and ItemService
    // ItemService is our own service factory created below.
    // $http is a default service created by Angular that handles our http
    app.controller('MainCtrl', ['$http','ItemService', function($http, ItemService){
        var self = this;

        // create an empty array to hold all the json objects we will get
        self.items = [];


        $http.get('http://localhost:3000/shoplist').then(function(res){
            self.items = res.data;
            ItemService.set(self.items);
            console.info(">> http response data: %s", res.data);
        }, function(err){
            console.error('http get error: ' + err);
        });

    }]);

    // the factory is setup for future use with routes and multi views
    // typically if we want to store data temporarily on the client's device
    // we would store it in a service like 'factory'
    app.factory('ItemService',[function(){
        var items = [];
        return {
            list: function(){
                return items;
            },
            add: function (i){
                items.push(i);
            },
            set: function(i){
                items = i;
            }

        };
    }]);

}());