// This is the server only. in this example, we have separated the server from the client.
// so unlike handlebars style of serving rendered HTML to clients, the server just gives
// whichever client is requesting for the address. currently in this case:
// http://localhost:3000/shoplist

// setup the express server
var express = require('express');
var app = express();

// favicon should be a 64x64 px .ico file
//var favicon = require('serve-favicon');
//app.use(favicon(__dirname + "/public/images/favicon.ico"));

// morgan will be used in the future to handle log errors
var logger = require('morgan');
app.use(logger('dev'));

// handlebars might be used in the future
//var handlebars = require('express-handlebars');

// used to parse data into json format
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// used to allow cross site access (cross origin resource sharing)
var cors = require('cors');
app.use(cors());

// expose folders
app.use(express.static(__dirname + "/bower_components"));
app.use(express.static(__dirname + "/public"));
//app.use(express.static(__dirname + "/routes"));

// our custom shoppinglist module that will handle the actual res.send(data) that we want
var shoplist = require('./routes/shoplist');
app.use('/shoplist', shoplist);

// default page
app.get('/', function(req,res){
   res.send('index');
});

//Catch all errors
app.use(function(req, res, next) {
    console.error("File not found: %s", req.originalUrl);
    //res.redirect("/");
    res.send("File not found: " + req.originalUrl);
});

app.set("port", process.env.APP_PORT || 3000);

//Start the server
app.listen(app.get("port"), function() {
    console.info("Application started on port %s", app.get("port"));
});