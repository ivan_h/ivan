var express = require('express');
var mysql = require('mysql');
var config = require('./../modules/dbconfig');
var bodyParser = require('body-parser');

var router = express.Router();
router.use(bodyParser.urlencoded({extended: false}));

var connPool = mysql.createPool(config);


router.get("/", function(req, res, next) {

    //Get a connection from the pool
    connPool.getConnection(function(err, conn) {
        //Check for error
        if (err) {
            res.send(err);
            return;
        }
        //Construct our SQL
        var paramQuery = "select * from `shoppinglist`.`list`";

        //Perform the query
        try {
            // in case you want to get data by parameter
            conn.query(paramQuery, // query
                [], //actual parameters based on position
                function (err, result) {
                    if (err) {
                        res.send(err);
                        return;
                    }
                    res.send(result);
                });
        } catch (ex) {
            res.send(err);
        } finally {
            conn.release();
        }
    });
});

module.exports = router;